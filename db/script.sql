BEGIN;

-- UPDATE TABLE "empresa" --------------------------------------
CREATE TABLE "empresa"(
	"id"     INTEGER NOT NULL PRIMARY KEY,
	"nombre" TEXT NOT NULL);

CREATE INDEX "index_empresa_id" ON "empresa"( "id" );

-- UPDATE TABLE "ruta" -----------------------------------------
CREATE TABLE "ruta"(
	"id"         INTEGER NOT NULL PRIMARY KEY,
	"numero"     INTEGER NOT NULL,
	"empresa_id" INTEGER,
	CONSTRAINT "pertenece" FOREIGN KEY ( "empresa_id" ) REFERENCES "empresa"( "id" )
		ON DELETE CASCADE
		ON UPDATE CASCADE);

CREATE INDEX "index_ruta_id" ON "ruta"( "id" );

-- CREATE TABLE "barrio" ---------------------------------------
CREATE TABLE "barrio"(
	"id" INTEGER NOT NULL PRIMARY KEY,
	"nombre" TEXT NOT NULL);
-- ---------------


-- CREATE INDEX "index_id1" ------------------------------------
CREATE INDEX "index_barrio_id" ON "barrio"( "id" );
-- -------------------------------------------------------------
----------------------------------------------


-- CREATE TABLE "trayecto" -------------------------------------
CREATE TABLE "trayecto"(
	"id" INTEGER NOT NULL,
	"tramo" INTEGER NOT NULL,
	"ruta_id" INTEGER NOT NULL,
	"barrio_id" INTEGER NOT NULL,
	CONSTRAINT "lnk_barrio_trayecto" FOREIGN KEY ( "barrio_id" ) REFERENCES "barrio"( "id" )
		ON DELETE CASCADE
		ON UPDATE CASCADE
,
	CONSTRAINT "lnk_ruta_trayecto" FOREIGN KEY ( "ruta_id" ) REFERENCES "ruta"( "id" )
		ON DELETE CASCADE
		ON UPDATE CASCADE
,
PRIMARY KEY ( "id", "ruta_id", "barrio_id", "tramo" ) );
-- -------------------------------------------------------------

-- CREATE INDEX "index_id_trayecto" ----------------------------
CREATE INDEX "index_trayecto_id" ON "trayecto"( "id" );
-- -------------------------------------------------------------


-- CREATE TABLE "bitacora_trayecto" ----------------------------
CREATE TABLE "public"."bitacora_trayecto" ( 
	"id" Serial NOT NULL,
	"barrio_inicio_id" Integer NOT NULL,
	"barrio_fin_id" Integer NOT NULL,
	"ruta_id" Integer NOT NULL,
	PRIMARY KEY ( "id" ),
	CONSTRAINT fk_bitacora_barrio_inicio FOREIGN KEY ( barrio_inicio_id) REFERENCES barrio(id),
	CONSTRAINT fk_bitacora_barrio_fin FOREIGN KEY ( barrio_fin_id) REFERENCES barrio(id)
);
-- -------------------------------------------------------------

-- CREATE TABLE "detalle_bitacora_trayecto" --------------------
CREATE TABLE "public"."detalle_bitacora_trayecto" ( 
	"id" Serial NOT NULL,
	"bitacora_trayecto_id" Integer NOT NULL,
	"barrio_id" Integer NOT NULL );
 ;
-- -------------------------------------------------------------

-- CHANGE "COMMENT" OF "FIELD "barrio_id" ----------------------
COMMENT ON COLUMN "public"."detalle_bitacora_trayecto"."barrio_id" IS 'Barrio por el que va pasando la ruta en el trayecto seleccionado.';
-- -------------------------------------------------------------

-- CREATE INDEX "index_id1" ------------------------------------
CREATE INDEX "index_id1" ON "public"."detalle_bitacora_trayecto" USING btree( "id" );


COMMIT;
