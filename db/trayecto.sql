﻿insert into barrio(nombre)
	values ('LA PAZ');

insert into barrio(nombre)
	values ('BELLAVISTA');

insert into barrio(nombre)
	values ('VILLA DEL VIENTO');

insert into barrio(nombre)
	values ('CAMPO BELLO');

insert into barrio(nombre)
	values ('PALACE');

insert into barrio(nombre)
	values ('CAMPANARIO');

insert into barrio(nombre)
	values ('LOS HOYOS');

insert into barrio(nombre)
	values ('POMONA');

insert into barrio(nombre)
	values ('TULCAN');

insert into barrio(nombre)
	values ('LA ESTANCIA');

insert into barrio(nombre)
	values ('BARRIO BOLVIBAR');

insert into barrio(nombre)
	values ('CENTRO');

insert into barrio(nombre)
	values ('SANTA INES');

insert into barrio(nombre)
	values ('MOSCOPAN');


/* Trayecto ruta uno de sotracauca ruta de la paz a 
   campanario. */

insert into trayecto (ruta_id, barrio_id, tramo)
	values (1, 1, 0);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (1, 2, 1);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (1, 3, 2);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (1, 4, 3);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (1, 5, 4);	

insert into trayecto (ruta_id, barrio_id, tramo)
	values (1, 6, 5);

/* Trayecto ruta cuatro de trans pubenza de la paz a campanario */
insert into trayecto (ruta_id, barrio_id, tramo)
	values (2, 1, 0);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (2, 2, 1);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (2, 3, 2);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (2, 4, 3);

insert into trayecto (ruta_id, barrio_id, tramo)
	values (2, 5, 4);	

insert into trayecto (ruta_id, barrio_id, tramo)
	values (2, 6, 5);	


/* CONSULTA PARA OBTENER LAS RUTAS QUE PASAN POR UN BARRIO DE INICIO Y UN BARRIO DE FIN. */

select r.id, r.numero, e.nombre 
from trayecto t1 
inner join ruta r on r.id = t1.ruta_id
inner join empresa e on e.id = r.empresa_id
where t1.barrio_id = 1
	and t1.ruta_id in(select t2.ruta_id
	from trayecto t2 where t2.barrio_id = 6 and t2.tramo > t1.tramo);


select b.nombre
from trayecto t inner join barrio b on t.barrio_id = b.id
where t.ruta_id = 1 and t.tramo between 
	(select tramo from trayecto where barrio_id = 1 and ruta_id = 1) and
	(select tramo from trayecto where barrio_id = 6 and ruta_id = 1);


select r.id, r.numero, e.nombre 
from trayecto t1 
inner join ruta r on r.id = t1.ruta_id
inner join empresa e on e.id = r.empresa_id
where t1.barrio_id = 1
	and t1.ruta_id in(select t2.ruta_id
	from trayecto t2 where t2.barrio_id = 6 and t2.tramo > t1.tramo);


select b.nombre
from trayecto t inner join barrio b on t.barrio_id = b.id
where t.ruta_id = 1 and t.tramo between 
	(select tramo from trayecto where barrio_id = 1 and ruta_id = 1) and
	(select tramo from trayecto where barrio_id = 7 and ruta_id = 1);

/* Insertar en la bitacora todas las busquedas realizadas por los usuarios,
   esto con el fin de realizar recomendaciones a las empresas sobre las 
   necesidades de los usuarios. */
insert into bitacora_busqueda
	(barrio_inicio_id, barrio_fin_id)
values (1, 7);
	

