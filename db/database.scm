#lang racket
; Contiene las funciones necesarias para interactuar con la base de datos

(require db)

(define pgc
    (postgresql-connect #:user "postgres"
                        #:database "routes"
                        #:password "3166215837"))


(provide get-barrios)
(provide get-trayecto)
(provide get-rutas)
(provide get-id-barrio)
(provide add_bitacora_busqueda)
(provide add_bitacora_trayecto)
(provide export-bitacora-trayecto)


(define get-barrios
  (lambda ()
    (query-list pgc
                "SELECT nombre FROM public.barrio")))

(define get-id-barrio
  (lambda (nombre-barrio)
    (query-value pgc
                 "select id from barrio where nombre = $1" nombre-barrio)))

(define get-id-rutasp
  (lambda (id-barrio-inicio id-barrio-fin)
    (map number->string (query-list pgc 
                "select r.id from trayecto t1 inner join ruta r on r.id = t1.ruta_id
                     inner join empresa e on e.id = r.empresa_id
                        where t1.barrio_id = $1
                        and t1.ruta_id in(select t2.ruta_id
                        from trayecto t2 where t2.barrio_id = $2 and t2.tramo > t1.tramo)" id-barrio-inicio id-barrio-fin))))

(define get-rutasp
  (lambda (id-barrio-inicio id-barrio-fin)
    (get-rutas-de-vector-list (get-rutasp1 id-barrio-inicio id-barrio-fin))))

(define get-rutasp1
  (lambda (id-barrio-inicio id-barrio-fin)
    (query-rows pgc
                "select r.id,concat('#', r.numero), e.nombre from trayecto t1 inner join ruta r on r.id = t1.ruta_id
                     inner join empresa e on e.id = r.empresa_id
                        where t1.barrio_id = $1
                        and t1.ruta_id in(select t2.ruta_id
                        from trayecto t2 where t2.barrio_id = $2 and t2.tramo > t1.tramo)" id-barrio-inicio id-barrio-fin)))

(define get-trayecto
  ; Muestra el recorrido por los barrios de una ruta con id 'id-ruta'
  ; entre los barrios con id 'id-barrio-inicio' y 'id-barrio-fin'
  (lambda (id-ruta id-barrio-inicio id-barrio-fin)
    (query-list pgc 
                "
select b.nombre
from trayecto t inner join barrio b on t.barrio_id = b.id
where t.ruta_id = $1 and (
	t.tramo >= (select tramo from trayecto where barrio_id = $2 and ruta_id = $1)
   and  t.tramo <= (select tramo from trayecto where barrio_id = $3 and ruta_id = $1))
   order by t.tramo;
" id-ruta id-barrio-inicio id-barrio-fin)))

(define get-rutas-de-vector-list
  (lambda (vector-list)
    (if (pair? vector-list)
        (cond
          ((null? (cdr vector-list))
           (list (vector->list (car vector-list))))
          (else (cons (vector->list (car vector-list)) (get-rutas-de-vector-list (cdr vector-list)))))
        '())))

(define get-string-of-list
  (lambda(ls acc)
    (cond
      ((null? ls)
       acc)
      ((if (string? (car ls))
       (string-append acc (car ls) " " (get-string-of-list (cdr ls) acc))
       (string-append acc (number->string (car ls)) " " (get-string-of-list (cdr ls) acc)))))))

(define get-rutas-in-string-list
  (lambda(ls)
    (get-string-of-list ls "")))

(define get-rutas
  (lambda (id-origen id-destino)
    (letrec ([rutas (get-rutasp id-origen id-destino)])
      (map get-rutas-in-string-list rutas))))


(define (add_bitacora_busqueda inicio fin)
  (query-exec pgc "insert into bitacora_busqueda (barrio_inicio_id,barrio_fin_id) values($1,$2)" inicio fin))


(define (add_bitacora_trayecto inicio fin id_ruta)
  (query-exec pgc "insert into bitacora_trayecto (barrio_inicio_id,barrio_fin_id,ruta_id) values($1,$2,$3)" inicio fin id_ruta))

(define get-info-bitacora-trayecto
  (lambda ()
    (query-list pgc
                "select concat('Se tomo ',count(*), ' veces la ruta ', r.numero, ' de la empresa ' ,e.nombre)
	from bitacora_trayecto b inner join ruta r on b.ruta_id = r.id
	inner join empresa e on e.id = r.empresa_id
	group by e.nombre, r.numero")))


(define get-info-bitacora-busqueda
  (lambda ()
    (query-list pgc
                "

")))

(define get-bitacora-trayecto
  (lambda()
    (map get-rutas-in-string-list (get-rutas-de-vector-list (get-info-bitacora-trayecto)))))

(define export-bitacora-trayecto
  (lambda()
    (call-with-output-file* "bitacoraTra.txt" #:mode 'text #:exists 'replace
                            (lambda (out) (display (get-info-bitacora-trayecto) out)))))
                            
(define export-bitacora-busqueda
  (lambda()
    (call-with-output-file* "bitacoraBusqueda.txt" #:mode 'text #:exists 'replace
                            (lambda (out) (display (get-info-bitacora-busqueda) out)))))