BEGIN;

-- UPDATE TABLE "empresa" --------------------------------------
CREATE TABLE "empresa"(
	"id"     INTEGER NOT NULL PRIMARY KEY,
	"nombre" TEXT NOT NULL);

CREATE INDEX "index_empresa_id" ON "empresa"( "id" );

-- UPDATE TABLE "ruta" -----------------------------------------
CREATE TABLE "ruta"(
	"id"         INTEGER NOT NULL PRIMARY KEY,
	"numero"     INTEGER NOT NULL,
	"empresa_id" INTEGER,
	CONSTRAINT "pertenece" FOREIGN KEY ( "empresa_id" ) REFERENCES "empresa"( "id" )
		ON DELETE CASCADE
		ON UPDATE CASCADE);

CREATE INDEX "index_ruta_id" ON "ruta"( "id" );

-- CREATE TABLE "barrio" ---------------------------------------
CREATE TABLE "barrio"(
	"id" INTEGER NOT NULL PRIMARY KEY,
	"nombre" TEXT NOT NULL);
-- ---------------


-- CREATE INDEX "index_id1" ------------------------------------
CREATE INDEX "index_barrio_id" ON "barrio"( "id" );
-- -------------------------------------------------------------
----------------------------------------------


-- CREATE TABLE "trayecto" -------------------------------------
CREATE TABLE "trayecto"(
	"id" INTEGER NOT NULL,
	"tramo" INTEGER NOT NULL,
	"ruta_id" INTEGER NOT NULL,
	"barrio_id" INTEGER NOT NULL,
	CONSTRAINT "lnk_barrio_trayecto" FOREIGN KEY ( "barrio_id" ) REFERENCES "barrio"( "id" )
		ON DELETE CASCADE
		ON UPDATE CASCADE
,
	CONSTRAINT "lnk_ruta_trayecto" FOREIGN KEY ( "ruta_id" ) REFERENCES "ruta"( "id" )
		ON DELETE CASCADE
		ON UPDATE CASCADE
,
PRIMARY KEY ( "id", "ruta_id", "barrio_id", "tramo" ) );
-- -------------------------------------------------------------

-- CREATE INDEX "index_id_trayecto" ----------------------------
CREATE INDEX "index_trayecto_id" ON "trayecto"( "id" );
-- -------------------------------------------------------------

-- CREATE TABLE "Bitacora" -------------------------------------
CREATE TABLE "Bitacora"(
	"id" INTEGER NOT NULL PRIMARY KEY,
	"ruta_id" INTEGER NOT NULL,
	"barrio_inicio_id" INTEGER NOT NULL,
	"barrio_fin_id" INTEGER NOT NULL,
	"fecha" DateTime NOT NULL );
-- -------------------------------------------------------------

-- CREATE INDEX "index_id_bitacora" ----------------------------
CREATE INDEX "index_id_bitacora" ON "Bitacora"( "id" );
-- -------------------------------------------------------------

COMMIT;